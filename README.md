My solution to answer [this StackOverflow question](https://stackoverflow.com/q/53880928/1577357).

My environment:

```
$ truffle version

Truffle v5.0.0 (core: 5.0.0)
Solidity v0.5.0 (solc-js)
Node v8.12.0
```

![](ganache-version.png)
