const Vault = artifacts.require("Vault");

contract("Vault test", async accounts => {
    
    // Rely on one instance for all tests.
    let vault;
    let fromAccount   = accounts[0];
    let oneEtherInWei = web3.utils.toWei('1', 'ether');

    // Runs before all tests.
    // https://mochajs.org/#hooks
    before(async () => {
        vault = await Vault.deployed();
    });

    // The `receipt` will return boolean.
    // https://web3js.readthedocs.io/en/1.0/web3-eth.html#gettransactionreceipt
    it("Test if 1 ether can be paid", async () => {
        let receipt = await web3.eth.sendTransaction({
            from:  fromAccount, 
            to:    vault.address, 
            value: oneEtherInWei
        });
        expect(receipt.status).to.equal(true);
    });

    it("Test if contract received 1 ether", async () => {
        let balance = await web3.eth.getBalance(vault.address);
        expect(balance).to.equal(oneEtherInWei);
    });

    // In Web3JS v1.0, `fromWei` will return string.
    // In order to use `at.least`, string needs to be parsed to integer.
    it("Test if balanceOf fromAccount is at least 1 ether in the contract", async () => {
        let balanceOf    = await vault.balanceOf.call(fromAccount);
        let balanceOfInt = parseInt(web3.utils.fromWei(balanceOf, 'ether'));
        expect(balanceOfInt).to.be.at.least(1);
    });
});
