pragma solidity >=0.4.21 <0.6.0;

contract Vault {
    mapping(address => uint256) public balanceOf;

    function() payable external {
        uint amount = msg.value;
        balanceOf[msg.sender] += amount;
    }
}
